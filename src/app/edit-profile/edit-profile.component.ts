// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-edit-profile',
//   templateUrl: './edit-profile.component.html',
//   styleUrls: ['./edit-profile.component.css']
// })
// export class EditProfileComponent {

//   urllink:string="insert/images/img3.jpg";

//   selectFiles(event:any){

//     if(event.target.files){

//       var reader=new FileReader()

//       reader.readAsDataURL(event.target.files[0])

//       reader.onload=(event:any)=>{

//         this.urllink=event.target.result

//       }

//     }

//   }
// }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  user: any = {
    profilePic: '',
    followers: 100,
    following: 50,
    username: 'YourUsername',
    email: 'youremail@example.com',
    password: 'YourPassword',
    contactNumber: '123-456-7890'
  };

  editUsername = false;
  editEmail = false;
  editPassword = false;
  editContact = false;
  showMoreInfo = false;

  constructor() { }

  ngOnInit(): void {
  }

  editProfilePic() {

  }

  viewFollowers() {

  }

  viewFollowing() {

  }

  toggleMoreInfo() {
    this.showMoreInfo = !this.showMoreInfo;
  }

  cancel() {
    
  }

  saveChanges() {
    // Add logic to save changes to user profile here
  }
}

