import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SpringConectionService } from '../serviceFolder/spring-conection.service';
import { Blog } from '../model/blog';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent  implements  OnInit{
  listArray:any=[];
  profileArray:any=[];

  javaCategory: Blog[] = [];
  pythonCategory: Blog[] = [];
  webtechCategory: Blog[] = [];
  dbCategory: Blog[] = [];

  constructor(private router:Router ,private urlService:SpringConectionService){
    this.listArray=[
      { name :'Java',id:1},
      { name :'Python',id:2},
      { name :'Webtech',id:3},
      { name :'Database',id:4},
     ]
     this.profileArray=[
      // { name :'Edit Profile',id:1},
      // { name :'Create a Blog',id:2},
      { name :'My Blogs',id:3},
      // { name :'Liked Blogs',id:4},
      { name :'Logout',id:5},
     ]
  }

  ngOnInit() {
    this.getAllJavaCategory();
    this.getAllPythonCategory();
    this.getAllWebtechCategory();
    this.getAllWebtechCategory();

  }
  Navigate(evt:any){
    if (evt === 1) {
      this.router.navigate(['/java'])
    } else if(evt === 2) {
      this.router.navigate(['/python'])
    }else if(evt === 3){
      this.router.navigate(['/webtech'])
    }else if(evt=== 4){
      this.router.navigate(['/database'])
    }

  }

  getAllJavaCategory(){
   //The .subscribe(...) method is used to listen for the response from the HTTP request.
    this.urlService.getAllJavaCat().subscribe((res:any)=>{
    //spread operator is used to add the elements individually to the javaCategory array.
      this.javaCategory.push(...res);
    })
  }

  getAllPythonCategory(){
    this.urlService.getAllPythonCat().subscribe((res:any)=>{
      this.pythonCategory.push(...res);
    })
  }

  getAllWebtechCategory(){
    this.urlService.getAllWebtechCat().subscribe((res:any)=>{
      this.webtechCategory.push(...res);
    })
  }

  getAllDbCategory(){
    this.urlService.getAllDbCat().subscribe((res:any)=>{
      this.dbCategory.push(...res);
    })
  }


  profileNavigate(evt:any)
  {
    if (evt === 1) {
      this.router.navigate(['/editprofile/'])
    } else if(evt === 2) {
      this.router.navigate(['/createblog/'])
    }else if(evt === 3){
      this.router.navigate(['/myblogs/'])
    }else if(evt=== 4){
      this.router.navigate(['/likedblogs/'])
    }else if(evt=== 5){
      this.router.navigate(['/login/'])
    }
  }
  submit(str:string)
  {
    this.router.navigate([str]);
  }
}
