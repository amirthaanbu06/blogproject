import { Component } from '@angular/core';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { BlogDTO } from 'src/app/model/dto';

@Component({
  selector: 'app-webtech',
  templateUrl: './webtech.component.html',
  styleUrls: ['./webtech.component.css']
})
export class WebtechComponent {
  webtechCategory: BlogDTO[] = [];
  blogDesc: BlogDTO[] = [];
  constructor(private urlService:SpringConectionService,private router: Router,private ActivateRoute:ActivatedRoute){

  }

   ngOnInit() {
    this.getAllWebtechCategory();

   }

   getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }

   getAllWebtechCategory(){
    this.urlService.getAllWebtechCat().subscribe((res:any)=>{
      this.webtechCategory.push(...res);
      console.log(this.webtechCategory)
    })
  }
}
