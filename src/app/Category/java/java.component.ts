import { Component , OnInit} from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { Blog } from 'src/app/model/blog';
import { BlogDTO } from 'src/app/model/dto';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
@Component({
  selector: 'app-java',
  templateUrl: './java.component.html',
  styleUrls: ['./java.component.css']
})
export class JavaComponent implements OnInit{

  javaCategory: BlogDTO[] = [];
  blogDesc: BlogDTO[] = [];
  SpringConectionService: any;

  errorMessage: string | undefined;
  constructor(private router:Router,private urlService:SpringConectionService){

 }

  ngOnInit() {
    this.getAllJavaCategory();

  }

  getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }

  getAllJavaCategory(){
    this.urlService.getAllJavaCat().subscribe((res:any)=>{
      this.javaCategory.push(...res);
      console.log(this.javaCategory)
    })
  }

  }



