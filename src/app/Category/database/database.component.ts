import { Component } from '@angular/core';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { BlogDTO } from 'src/app/model/dto';

@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.css']
})
export class DatabaseComponent {

  dbCategory: BlogDTO[] = [];
  blogDesc: BlogDTO[] = [];


  constructor(private urlService:SpringConectionService,private router: Router){
  }

   ngOnInit() {
    this.getAllDbCategory();
   }

   getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }

   getAllDbCategory(){
    this.urlService.getAllDbCat().subscribe((res:any)=>{
      this.dbCategory.push(...res);
      console.log(this.dbCategory)
    })
  }

}
