import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Component } from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms'
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Register } from 'src/app/model/register';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';



function phoneNumberValidator(control: FormControl): { [key: string]: boolean } | null {
  const phoneNumber = control.value;
  const valid = /^\d{10}$/.test(phoneNumber); // Check if it's a 10-digit number

  return valid ? null : { invalidPhoneNumber: true };
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  author:string='';
  title = 'RegisterForm';
  register=new FormGroup({
    name:new FormControl("",Validators.required),
    email:new FormControl("",Validators.email),
    password:new FormControl("",Validators.minLength(6)),
    contactNumber: new FormControl("", [Validators.required, phoneNumberValidator])
  })

  userRegisterationModel:Register=new Register();
  constructor(public urlService:SpringConectionService,private router: Router,private toastr: ToastrService  ){

  }

  get vname(){
    return this.register.get("name")
  }
  get vpassword(){
    return this.register.get("password")
  }
  get vemail(){
    return this.register.get("email")
  }
  get vnumber(){
    return this.register.get("contactNumber")
  }

  getData(){
    // console.log(this.userRegisterationModel)
    this.urlService.createRegistration(this.register.value).subscribe((_data)=>{
      //localstorage
      // localStorage.setItem('username',JSON.stringify(this.userRegisterationModel))
    });

  }
  registervaild(){
    if(this.register.valid){
      this.getData();
      this.router.navigateByUrl('/login');
      // this.toastr.success("registered")
    }
    else{
      console.log('hsdj');
      // this.toastr.success("not registered")
    }
  }
}
