import { Component } from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Register } from 'src/app/model/register';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  resmsg!:string;
  title = 'LoginForm';
  login=new FormGroup({
    email:new FormControl("",Validators.email),
    password:new FormControl("",Validators.minLength(6)),
  })

  reg = new Register();

  constructor(private  urlService:SpringConectionService,private router: Router,private toastr: ToastrService) {}

  logindata() {
    this.urlService.loginuser(this.login.value).subscribe((data: any) => {
        // console.log(data);
        this.resmsg=data.returnResponce;
        if(this.resmsg==="Successfully"){
          sessionStorage.setItem("userId", data.userId);
          this.toastr.success("login successfully")
          this.router.navigate(['/home']);

        }
        else{
          console.log("asdfg");
        }

      },
    );


  }

  get vpassword(){
    return this.login.get("password")
  }
  get vemail(){
    return this.login.get("email")
  }

}
