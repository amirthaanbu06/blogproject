import { Component , OnInit} from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { BlogDTO } from '../model/dto';

@Component({
  selector: 'app-my-blog',
  templateUrl: './my-blog.component.html',
  styleUrls: ['./my-blog.component.css']
})
export class MyBlogComponent {
  myBlogs: BlogDTO[] = [];
   userId: any;
   blogDesc: BlogDTO[] = [];
  //  blogId:any;


  constructor(private route:Router,private urlService:SpringConectionService,private router: ActivatedRoute){
    this.router.params.subscribe(params => {
      console.log(333333);

      this.userId = params['userId'];
    });
  }
  ngOnInit() {
    console.log(44444);

    this.getMyBlogs(this.userId);
   }

  //  getBlogDescription(blogTitle: any) {
  //   this.router.navigate([`/blogs/${blogTitle}`])
  // }
  
   getMyBlogs(userId: any) {
    console.log('entered blog desc');
    this.urlService.getMyBlogs(userId).subscribe((res: any) => {
      this.myBlogs.push(...res);
      console.log(777777, res);
    });
  }

  deleteMyBlogs(blogId: any){
    this.urlService.deleteMyBlogs(blogId).subscribe((res: any) => {
      // this.myBlogs.push(...res);
      const index = this.myBlogs.findIndex(blog => blog.blogId === blogId);
      if (index !== -1) {
        // Remove the blog from 'myBlogs'
        this.myBlogs.splice(index, 1);
      }
      // console.log(8888888,blogId);

      console.log(777777, res);
    });
  }




 }
