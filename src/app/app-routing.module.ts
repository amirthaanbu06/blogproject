import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './form/login/login.component';
import { RegisterComponent } from './form/register/register.component';
import { HeaderComponent } from './header/header.component';
import { JavaComponent } from './Category/java/java.component';
import { PythonComponent } from './Category/python/python.component';
import { WebtechComponent } from './Category/webtech/webtech.component';
import { DatabaseComponent } from './Category/database/database.component';
import { CreateblogComponent } from './createblog/createblog.component';
import { HomeComponent } from './home/home.component';
import { WelcomepageComponent } from './welcomepage/welcomepage.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyBlogComponent } from './my-blog/my-blog.component';
import { LikedBlogsComponent } from './liked-blogs/liked-blogs.component';
import { PopularComponent } from './popular/popular.component';
import { BlogDescriptionComponent } from './blog-description/blog-description.component';



export const routes: Routes = [
  { path:'welcome',component:WelcomepageComponent},
  {path:'',redirectTo:'welcome',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  // {path:'home/category/java',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'header',component:HeaderComponent},
  {path:'createblog',component:CreateblogComponent},
  {path:'java',component:JavaComponent},
  {path:'python',component:PythonComponent},
  {path:'webtech',component:WebtechComponent},
  {path:'database',component:DatabaseComponent},
  {path:'editprofile',component:EditProfileComponent},
  {path:'myblogs',component:MyBlogComponent},
  {path:'likedblogs',component:LikedBlogsComponent},
  {path:'java/see_all_articles',component:JavaComponent},
  {path:'python/see_all_articles',component:PythonComponent},
  {path:'webtech/see_all_articles',component:WebtechComponent},
  {path:'database/see_all_articles',component:DatabaseComponent},
  {path:'popular/see_all_articles',component:PopularComponent},
  {path:'blogs/:blogTitle',component:BlogDescriptionComponent},
  {path:'my_blogs/:userId',component:MyBlogComponent},
  {path:'my_blogs/delete/:blogId',component:MyBlogComponent}

  //  {path:'**',redirectTo:'home',pathMatch:'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
