import { Component } from '@angular/core';
import { BlogDTO } from '../model/dto';
import { ActivatedRoute, Router } from '@angular/router';
import { SpringConectionService } from '../serviceFolder/spring-conection.service';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent {
  popularArticles: BlogDTO[] = [];
  blogDesc: BlogDTO[] = [];
  SpringConectionService: any;

  errorMessage: string | undefined;
  constructor(private router:Router,private urlService:SpringConectionService,private ActivateRoute:ActivatedRoute){

 }

  ngOnInit() {
    this.getAllPopularArticles();

  }

  getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }

  getAllPopularArticles(){
    this.urlService.getAllPopulars().subscribe((res:any)=>{
      this.popularArticles.push(...res);
      console.log(this.popularArticles)
    })
  }

}
