import { Category } from "./category";

export class Blog {
    blogId?:number;
    blogTitle?:String;
    blogDescription?:String;
    likes?:number;
    comments?:number;
    author?:String;
    followers?:number;
    timestamp?:String;
    catId?:any;
    userId?:any;
}
