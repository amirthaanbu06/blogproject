import { Category } from "./category";

export class BlogDTO {
    blogId?:number;
    blogTitle?:String;
    blogDescription?:String;
    likes?:number;
    comments?:number;
    author?:String;
    timestamp?:String;
    catId?:any;
    userId?:any;
    formattedTimestamp?:any;
}
