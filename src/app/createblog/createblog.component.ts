import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators ,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { SpringConectionService } from '../serviceFolder/spring-conection.service';
import { Category } from '../model/category';
import { Blog } from '../model/blog';


@Component({
  selector: 'app-createblog',
  templateUrl: './createblog.component.html',
  styleUrls: ['./createblog.component.css']
})
export class CreateblogComponent implements OnInit {

  // firstForm! :FormGroup
  post!:FormGroup
  author:string='';


  userCreateBlog :Blog=new Blog();
  addBlog!:String|undefined;
  listArray:any[]=[]
  constructor(private urlService:SpringConectionService,private router: Router,private fb:FormBuilder){
    this.post=this.fb.group({
      blogTitle:new FormControl("",Validators.required),
      blogDescription:new FormControl("",Validators.required),
      catId:new FormControl("",Validators.required),
      author:new FormControl("",Validators.required),
    })
    this.listArray=[
      { name :'Java',id:1},
      { name :'Python',id:2},
      { name :'Webtech',id:3},
      { name :'Database',id:4}
     ]

    //  let user=localStorage.getItem('username');
    //  let username=user&&JSON.parse(user).name;
    //  if(username!=null){
    //    this.author=username;
    //    console.log(this.author);
    //  }


  }
  ngOnInit(){

  }

  navicate(){
    this.getpost();
    this.router.navigateByUrl('/home');
  }
  getId(evt:any){
  console.log(33333);
  const getIdnty = this.listArray.find((ele:any)=> ele.name === evt.target.value);
  if(getIdnty){
    this.post.controls['catId'].setValue(getIdnty.id);
    this.userCreateBlog.catId= getIdnty.name
  }
  }
  getpost(){
    if(this.post.status === 'VALID'){
      let object:Blog ={
      blogTitle:this.post.controls['blogTitle'].value,
      blogDescription:this.post.controls['blogDescription'].value,
      catId:this.post.controls['catId'].value,
      author:this.post.controls['author'].value
      }
      this.urlService.createBlog(object).subscribe((result)=>{
        console.log(result);
        if(result){
          alert("Blog added sucessuly")
        }
      },
      (error)=>{
        this.addBlog="Blog added failed"
      }

    );
    }else{
     alert('form not valid')
    }

  }


}



