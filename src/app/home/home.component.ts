import { CreateblogComponent } from '../createblog/createblog.component';
import { Component , OnInit} from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { BlogDTO } from '../model/dto';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit{
  categoryArray:any=[];

  javaImgLink:any='https://www.gcreddy.com/wp-content/uploads/2021/05/Java-Programming-Language-1.png'

  pythonImgLink:any='https://developers.redhat.com/sites/default/files/styles/article_feature/public/Python-01%20%282%29.webp?itok=TEVIQBQo'

  webtechImgLink:any='https://i0.wp.com/blog.oureducation.in/wp-content/uploads/2013/03/7074702-web-technology-internet-abstract-as-a-concept.jpg?resize=300%2C183&ssl=1'

  dbImgLink:any='https://assets.toptal.io/images?url=https%3A%2F%2Fbs-uploads.toptal.io%2Fblackfish-uploads%2Fcomponents%2Fblog_post_page%2Fcontent%2Fcover_image_file%2Fcover_image%2F1282568%2Fregular_1708x683_0712-Bad_Practices_in_Database_Design_-_Are_You_Making_These_Mistakes_Dan_Newsletter-f90d29e5d2384eab9f4f76a0a18fa9a8.png'

  blogDesc: BlogDTO[] = [];
  myBlogs: BlogDTO[] = [];
  popularCategory: BlogDTO[] = [];
  javaCategory: Blog[] = [];
  pythonCategory: Blog[] = [];
  webtechCategory: Blog[] = [];
  dbCategory: Blog[] = [];
  popularArticles: BlogDTO[] = [];
  javaArticles: BlogDTO[] = [];
  pythonArticles: BlogDTO[] = [];
  webtechArticles: BlogDTO[] = [];
  dbArticles: BlogDTO[] = [];
  constructor(private router:Router,private urlService:SpringConectionService,private ActivateRoute:ActivatedRoute){
    this.categoryArray=[
      { name :'Java', img:this.javaImgLink ,id:1},
      { name :'Python',img:this.pythonImgLink, id:2},
      { name :'Webtech',img:this.webtechImgLink ,id:3},
      { name :'Database', img:this.dbImgLink ,id:4},
    ]


    }
    Navigate(evt:any){
      if (evt === 1) {
        this.router.navigate(['/java'])
      } else if(evt === 2) {
        this.router.navigate(['/python'])
      }else if(evt === 3){
        this.router.navigate(['/webtech'])
      }else if(evt=== 4){
        this.router.navigate(['/database'])
      }
      else if(evt=== 5){
        this.router.navigate(['/popular/see_all_articles'])
      }
      else if(evt=== 6){
        this.router.navigate(['/java/see_all_articles'])
      }
      else if(evt=== 7){
        this.router.navigate(['/python/see_all_articles'])
      }
      else if(evt=== 8){
        this.router.navigate(['/webtech/see_all_articles'])
      }
      else if(evt=== 9){
        this.router.navigate(['/database/see_all_articles'])
      }

    }
 ngOnInit() {
  this.getAllJavaCategory();
  this.getAllPythonCategory();
  this.getAllWebtechCategory();
  this.getAllDbCategory();
  this.getPopularTop4();
  this.getJavaTop4();
  this.getPythonTop4();
  this.getWebtechTop4();
  this.getDbTop4();

  }

  getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }

  getMyBlogs(userId: any) {
    this.router.navigate([`/my_blogs/${userId}`])
  }

  getAllJavaCategory(){
    //The .subscribe(...) method is used to listen for the response from the HTTP request.
    this.urlService.getAllJavaCat().subscribe((res:any)=>{
   //spread operator is used to add the elements individually to the javaCategory array.
    this.javaCategory.push(...res);

    })
  }

  getAllPythonCategory(){
    this.urlService.getAllPythonCat().subscribe((res:any)=>{
      this.pythonCategory.push(...res);
    })
  }

  getAllWebtechCategory(){
    this.urlService.getAllWebtechCat().subscribe((res:any)=>{
      this.webtechCategory.push(...res);
    })
  }

  getAllDbCategory(){
    this.urlService.getAllDbCat().subscribe((res:any)=>{
      this.dbCategory.push(...res);
    })
  }

  getPopularTop4(){
      this.urlService.getPopulars().subscribe((res:any)=>{
        this.popularArticles.push(...res);
      })
  }

  getJavaTop4(){
    this.urlService.getJava().subscribe((res:any)=>{
      this.javaArticles.push(...res);
    })
  }

  getPythonTop4(){
    this.urlService.getPython().subscribe((res:any)=>{
      this.pythonArticles.push(...res);
    })
  }

  getWebtechTop4(){
    this.urlService.getWebtech().subscribe((res:any)=>{
      this.webtechArticles.push(...res);
    })
  }

  getDbTop4(){
    this.urlService.getDb().subscribe((res:any)=>{
      this.dbArticles.push(...res);
    })
  }

}
