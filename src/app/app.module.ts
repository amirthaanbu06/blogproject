import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './form/login/login.component';
import { RegisterComponent } from './form/register/register.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JavaComponent } from './Category/java/java.component';
import { PythonComponent } from './Category/python/python.component';
import { WebtechComponent } from './Category/webtech/webtech.component';
import { DatabaseComponent } from './Category/database/database.component';
import { CreateblogComponent } from './createblog/createblog.component';
import { HomeComponent } from './home/home.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { WelcomepageComponent } from './welcomepage/welcomepage.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyBlogComponent } from './my-blog/my-blog.component';
import { LikedBlogsComponent } from './liked-blogs/liked-blogs.component';
import { PopularComponent } from './popular/popular.component';
import { BlogDescriptionComponent } from './blog-description/blog-description.component';
import {ToastrModule} from 'ngx-toastr'





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    JavaComponent,
    PythonComponent,
    WebtechComponent,
    DatabaseComponent,
    CreateblogComponent,
    HomeComponent,
    WelcomepageComponent,
    EditProfileComponent,
    MyBlogComponent,
    LikedBlogsComponent,
    PopularComponent,
    BlogDescriptionComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,ReactiveFormsModule,
    FormsModule,HttpClientModule,
     BrowserAnimationsModule,
     MatSlideToggleModule,
     MatInputModule,
     MatFormFieldModule,
     MatIconModule,
     MatSidenavModule,
     ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
